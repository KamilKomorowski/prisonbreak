using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

public class PlayerManager : MonoBehaviour
{
    private Inventory inventory;
    public float initialMaxWeight=100;
    public Transform directionSetter;

    void Start()
    {
        inventory = new Inventory(initialMaxWeight);
    }

    void Update()
    {
        if (Input.GetButtonDown("Interact"))
        {
            RaycastHit hit;
            Debug.DrawLine(directionSetter.position, directionSetter.position + directionSetter.forward * 2f, Color.blue, 1f);
            
            
            if (Physics.SphereCast(transform.position, 0.2f, directionSetter.forward, out hit, 2))
            {
                Debug.Log(hit.collider.gameObject.name);
                IInteractable i = hit.collider.gameObject.GetComponent<IInteractable>();
                if (i != null)
                {
                    i.Action(this);
                }

                if (hit.collider.gameObject.name == "decorative_half_wall_4_LOD")
                {
                    GameObject.Find("decorative_half_wall_4_LOD").GetComponent<Lights>().onoff = true;
                }
            }
        }
        
        if (Input.GetKeyDown(KeyCode.O))
        {
            GameObject.Find("APIdoor").GetComponent<APIConnection>().open = true;
        }
    }

    public void DropItem(string name)
    {
        Item i = inventory.GetItemWithName(name);

        if (i != null)
        {
            inventory.RemoveItem(i);
            GameManager.Instance.DropItem(name, transform.position + transform.forward);
        }
        GameManager.Instance.TriggerInventoryUIUpdate();
    }
    
    public bool AddItem(Item i)
    {
        bool success = inventory.AddItem(i);
        if (success)
        {
            GameManager.Instance.TriggerInventoryUIUpdate();
        }
        return success;
        
    }

    public bool CanOpenDoor(int id)
    {
        return inventory.CanOpenDoor(id);
    }

    public string[] GetItemNames()
    {
        return inventory.GetItemNames();
    }
     
}
