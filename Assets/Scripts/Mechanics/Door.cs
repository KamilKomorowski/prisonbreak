using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IInteractable
{
    public int id;
    public bool open = false;
    public float initialRotation;
    
    // Start is called before the first frame update
    void Start()
    {
        gameObject.tag = "Interactable";
        initialRotation = transform.localRotation.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (open && transform.localRotation.eulerAngles.y < initialRotation + 80)
        {
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(0, initialRotation + 80, 0), 5);
  
        } 
        else if (!open && transform.localRotation.eulerAngles.y > initialRotation)
        {
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(0, initialRotation, 0), 5);
        }
        
    }

    public void Open()
    {
        open = !open;
    }

    public void Action(PlayerManager player)
    {
        if (player.CanOpenDoor(id))
        {
            Open();
        }
    }
}
