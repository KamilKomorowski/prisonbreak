using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights : MonoBehaviour
{
    public GameObject door;
    public GameObject lights;
    public bool onoff;
    
    public Transform directionSetter;

    
    // Start is called before the first frame update
    void Start()
    {
        onoff = false;
        lights.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (onoff == true)
        {
            lights.SetActive(true);
            Destroy(door);
        }
    }
    
}
