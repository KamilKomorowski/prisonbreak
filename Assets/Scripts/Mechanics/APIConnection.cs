using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;

public class APIConnection : MonoBehaviour
{
    public string Outcome;
    
    public bool open = false;
    public float initialRotation;


    IEnumerator GetRequest(string url)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError(": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log(":\nReceived: " + webRequest.downloadHandler.text);
                    Outcome = webRequest.downloadHandler.text.Substring(1,webRequest.downloadHandler.text.Length - 2);
                    break;
            }
        }
    }
    
    void Start()
    {
        initialRotation = transform.localRotation.eulerAngles.y;
    }

    void Update()
    {
        if (open == true)
        {
            FlipCoin();
        }
    }
    
    public void FlipCoin()
    {
        StartCoroutine(GetRequest("http://flipacoinapi.com/json"));

        if (Outcome == "Heads")
        {
            
            if (transform.localRotation.eulerAngles.y < initialRotation + 80)
            {
                transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(0, initialRotation + 80, 0), 78);
            }

            open = true;
        }
        else
        {
            open = false;
        }
    }
}